> summary(paris_eM1)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ paris_ev + (1 | ID)
   Data: myD

REML criterion at convergence: 5942.2

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.6783 -0.5518 -0.1862  0.3342  4.0083 

Random effects:
 Groups   Name        Variance Std.Dev.
 ID       (Intercept) 175.8    13.26   
 Residual             365.5    19.12   
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   14.305      1.698   8.426
paris_ev      61.871      3.958  15.633

Correlation of Fixed Effects:
         (Intr)
paris_ev -0.409
> summary(paris_eM2)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ paris_ev + (paris_ev | ID)
   Data: myD

REML criterion at convergence: 5919.7

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.5671 -0.5181 -0.1670  0.2972  3.9347 

Random effects:
 Groups   Name        Variance Std.Dev. Corr 
 ID       (Intercept)  244.8   15.64         
          paris_ev    1278.2   35.75    -0.51
 Residual              313.6   17.71         
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   14.305      1.861   7.688
paris_ev      61.871      5.186  11.930

Correlation of Fixed Effects:
         (Intr)
paris_ev -0.558
> summary(dfh_eM1)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dfh_ev + (1 | ID)
   Data: myD

REML criterion at convergence: 5973.2

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.2931 -0.6002 -0.1804  0.3583  4.0411 

Random effects:
 Groups   Name        Variance Std.Dev.
 ID       (Intercept) 172.9    13.15   
 Residual             385.6    19.64   
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   11.626      1.819   6.392
dfh_ev        45.359      3.193  14.208

Correlation of Fixed Effects:
       (Intr)
dfh_ev -0.524
> summary(dfh_eM2)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dfh_ev + (dfh_ev | ID)
   Data: myD

REML criterion at convergence: 5963.3

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.4100 -0.5850 -0.1546  0.3698  3.8147 

Random effects:
 Groups   Name        Variance Std.Dev. Corr 
 ID       (Intercept) 237.1    15.40         
          dfh_ev      535.0    23.13    -0.50
 Residual             350.4    18.72         
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   11.626      1.961   5.927
dfh_ev        45.359      3.859  11.753

Correlation of Fixed Effects:
       (Intr)
dfh_ev -0.614
> summary(dp_eM1)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dp_ev + (1 | ID)
   Data: myD

REML criterion at convergence: 6026.5

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.5673 -0.6388 -0.2386  0.4736  3.6887 

Random effects:
 Groups   Name        Variance Std.Dev.
 ID       (Intercept) 167.5    12.94   
 Residual             423.3    20.57   
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   21.976      1.574   13.97
dp_ev         33.959      2.942   11.54

Correlation of Fixed Effects:
      (Intr)
dp_ev -0.176
> summary(dp_eM2)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dp_ev + (dp_ev | ID)
   Data: myD

REML criterion at convergence: 6026.1

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.5910 -0.6297 -0.2265  0.4926  3.6411 

Random effects:
 Groups   Name        Variance Std.Dev. Corr 
 ID       (Intercept) 169.67   13.026        
          dp_ev        92.08    9.596   -0.08
 Residual             415.46   20.383        
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   21.976      1.577   13.94
dp_ev         33.959      3.076   11.04

Correlation of Fixed Effects:
      (Intr)
dp_ev -0.186
