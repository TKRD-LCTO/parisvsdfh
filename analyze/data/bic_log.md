> ### 適宜変えてね
> path <- "~/programs/parisvsdfh/analyze/data"

> file <- "cleansing_all_add_variables.csv"

> ### 適宜変えてね
> 
> setwd(path)

> myD <- read.csv(file)

> #LMER 素のデータ
> paris_M1 <- lmer(rawrating ~ paris_v + (1|ID), data=myD)

> paris_M2 <- lmer(rawrating ~ paris_v + (paris_v|ID), data=myD)

> dfh_M1 <- lmer(rawrating ~ dfh_v + (1|ID), data=myD)

> dfh_M2 <- lmer(rawrating ~ dfh_v + (dfh_v|ID), data=myD)

> dp_M1 <- lmer(rawrating ~ dp_v + (1|ID), data=myD)

> dp_M2 <- lmer(rawrating ~ dp_v + (dp_v|ID), data=myD)

> #LMERDFHがNANになる刺激組Q36,Q42を除外
> paris_exM1 <- lmer(rawrating ~ paris_v + (1|ID), data=myD[1:476,])

> paris_exM2 <- lmer(rawrating ~ paris_v + (paris_v|ID), data=myD[1:476,])

> dfh_exM1 <- lmer(rawrating ~ dfh_v + (1|ID), data=myD[1:476,])

> dfh_exM2 <- lmer(rawrating ~ dfh_v + (dfh_v|ID), data=myD[1:476,])

> dp_exM1 <- lmer(rawrating ~ dp_v + (1|ID), data=myD[1:476,])

> dp_exM2 <- lmer(rawrating ~ dp_v + (dp_v|ID), data=myD[1:476,])

> #LMER すべての刺激組に+1
> paris_1M1 <- lmer(rawrating ~ paris_1v + (1|ID), data=myD)

> paris_1M2 <- lmer(rawrating ~ paris_1v + (paris_1v|ID), data=myD)

> dfh_1M1 <- lmer(rawrating ~ dfh_1v + (1|ID), data=myD)

> dfh_1M2 <- lmer(rawrating ~ dfh_1v + (dfh_1v|ID), data=myD)

> dp_1M1 <- lmer(rawrating ~ dp_1v + (1|ID), data=myD)

> dp_1M2 <- lmer(rawrating ~ dp_1v + (dp_1v|ID), data=myD)

> #LMER すべての刺激組に+0.1
> paris_01M1 <- lmer(rawrating ~ paris_01v + (1|ID), data=myD)

> paris_01M2 <- lmer(rawrating ~ paris_01v + (paris_01v|ID), data=myD)

> dfh_01M1 <- lmer(rawrating ~ dfh_01v + (1|ID), data=myD)

> dfh_01M2 <- lmer(rawrating ~ dfh_01v + (dfh_01v|ID), data=myD)

> dp_01M1 <- lmer(rawrating ~ dp_01v + (1|ID), data=myD)

> dp_01M2 <- lmer(rawrating ~ dp_01v + (dp_01v|ID), data=myD)

> #LMER すべての刺激組に+10^-100
> paris_eM1 <- lmer(rawrating ~ paris_ev + (1|ID), data=myD)

> paris_eM2 <- lmer(rawrating ~ paris_ev + (paris_ev|ID), data=myD)

> dfh_eM1 <- lmer(rawrating ~ dfh_ev + (1|ID), data=myD)

> dfh_eM2 <- lmer(rawrating ~ dfh_ev + (dfh_ev|ID), data=myD)

> dp_eM1 <- lmer(rawrating ~ dp_ev + (1|ID), data=myD)

> dp_eM2 <- lmer(rawrating ~ dp_ev + (dp_ev|ID), data=myD)

> #### BIC 算出 ###
> 
> BIC(paris_M1)
[1] 5968.19

> BIC(paris_M2)
[1] 5958.722

> BIC(dfh_M1)
[1] 4265.069

> BIC(dfh_M2)
[1] 4267.662

> BIC(dp_M1)
[1] 4334.848

> BIC(dp_M2)
[1] 4347.034

> bic_raw <- c(BIC(paris_M1),BIC(paris_M2),BIC(dfh_M1),BIC(dfh_M2),BIC(dp_M1),BIC(dp_M2))

> BIC(paris_exM1)
[1] 4273.217

> BIC(paris_exM2)
[1] 4268.197

> BIC(dfh_exM1)
[1] 4265.069

> BIC(dfh_exM2)
[1] 4267.662

> BIC(dp_exM1)
[1] 4334.848

> BIC(dp_exM2)
[1] 4347.034

> bic_ex <- c(BIC(paris_exM1),BIC(paris_exM2),BIC(dfh_exM1),BIC(dfh_exM2),BIC(dp_exM1),BIC(dp_exM2))

> BIC(paris_1M1)
[1] 5969.093

> BIC(paris_1M2)
[1] 5959.218

> BIC(dfh_1M1)
[1] 5972.674

> BIC(dfh_1M2)
[1] 5965.963

> BIC(dp_1M1)
[1] 5986.203

> BIC(dp_1M2)
[1] 5982.079

> bic_1 <- c(BIC(paris_1M1),BIC(paris_1M2),BIC(dfh_1M1),BIC(dfh_1M2),BIC(dp_1M1),BIC(dp_1M2))

> BIC(paris_01M1)
[1] 5968.284

> BIC(paris_01M2)
[1] 5958.687

> BIC(dfh_01M1)
[1] 5986.656

> BIC(dfh_01M2)
[1] 5986.225

> BIC(dp_01M1)
[1] 6035.68

> BIC(dp_01M2)
[1] 6046.419

> bic_01 <- c(BIC(paris_01M1),BIC(paris_01M2),BIC(dfh_01M1),BIC(dfh_01M2),BIC(dp_01M1),BIC(dp_01M2))

> BIC(paris_eM1)
[1] 5968.191

> BIC(paris_eM2)
[1] 5958.667

> BIC(dfh_eM1)
[1] 5999.231

> BIC(dfh_eM2)
[1] 6002.31

> BIC(dp_eM1)
[1] 6052.524

> BIC(dp_eM2)
[1] 6065.053

> bic_e <- c(BIC(paris_eM1),BIC(paris_eM2),BIC(dfh_eM1),BIC(dfh_eM2),BIC(dp_eM1),BIC(dp_eM2))

> #### BIC 算出 ####
> 
> 
> 
> ### plot ###
> 
> model_bic <- cbind(bic_raw,bic_ex,bic_1,bic_01,bic_e)

> xnames <- c("paris_M1","paris_M2","dfh_M1","dfh_M2","dp_M1","dp_M2")

> plottitlename <- "BIC"

> titles <- c("raw","ex","1","01","e")

> for(i in 1:length(model_bic[1,])){
+     t = paste(i , plottitlename, titles[i],sep="_")
+     png(paste(t,".png", sep=""),width=600,height=600)
+   .... [TRUNCATED] 

> ### plot ###
> 
> 
> 
> 
> 
