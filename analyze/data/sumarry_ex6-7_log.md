> summary(paris_M1)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ paris_v + (1 | ID)
   Data: myD

REML criterion at convergence: 5942.2

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.6764 -0.5543 -0.1895  0.3336  4.0072 

Random effects:
 Groups   Name        Variance Std.Dev.
 ID       (Intercept) 175.2    13.24   
 Residual             365.6    19.12   
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   14.329      1.696   8.451
paris_v       61.837      3.956  15.630

Correlation of Fixed Effects:
        (Intr)
paris_v -0.409
> summary(paris_M2)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ paris_v + (paris_v | ID)
   Data: myD

REML criterion at convergence: 5919.7

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.5656 -0.5181 -0.1683  0.2968  3.9340 

Random effects:
 Groups   Name        Variance Std.Dev. Corr 
 ID       (Intercept)  244.1   15.62         
          paris_v     1277.3   35.74    -0.51
 Residual              313.7   17.71         
Number of obs: 665, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   14.327      1.858   7.709
paris_v       61.841      5.184  11.928

Correlation of Fixed Effects:
        (Intr)
paris_v -0.558
> summary(dfh_M1)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dfh_v + (1 | ID)
   Data: myD

REML criterion at convergence: 4240.4

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.5906 -0.5825 -0.2023  0.4569  3.1047 

Random effects:
 Groups   Name        Variance Std.Dev.
 ID       (Intercept) 184.7    13.59   
 Residual             355.2    18.85   
Number of obs: 474, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)    1.813      2.662   0.681
dfh_v         65.436      5.014  13.052

Correlation of Fixed Effects:
      (Intr)
dfh_v -0.787
> summary(dfh_M2)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dfh_v + (dfh_v | ID)
   Data: myD

REML criterion at convergence: 4230.7

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.7946 -0.5404 -0.1602  0.4666  3.2305 

Random effects:
 Groups   Name        Variance Std.Dev. Corr 
 ID       (Intercept)  435.8   20.88         
          dfh_v       1347.0   36.70    -0.74
 Residual              305.4   17.47         
Number of obs: 474, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)    1.815      3.002   0.605
dfh_v         65.434      5.983  10.937

Correlation of Fixed Effects:
      (Intr)
dfh_v -0.837
> summary(dp_M1)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dp_v + (1 | ID)
   Data: myD

REML criterion at convergence: 4310.2

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.6452 -0.6712 -0.2021  0.4963  2.9154 

Random effects:
 Groups   Name        Variance Std.Dev.
 ID       (Intercept) 170.4    13.05   
 Residual             426.1    20.64   
Number of obs: 474, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)   23.989      1.741  13.775
dp_v          30.174      3.394   8.891

Correlation of Fixed Effects:
     (Intr)
dp_v -0.335
> summary(dfh_M2)
Linear mixed model fit by REML ['lmerMod']
Formula: rawrating ~ dfh_v + (dfh_v | ID)
   Data: myD

REML criterion at convergence: 4230.7

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.7946 -0.5404 -0.1602  0.4666  3.2305 

Random effects:
 Groups   Name        Variance Std.Dev. Corr 
 ID       (Intercept)  435.8   20.88         
          dfh_v       1347.0   36.70    -0.74
 Residual              305.4   17.47         
Number of obs: 474, groups:  ID, 95

Fixed effects:
            Estimate Std. Error t value
(Intercept)    1.815      3.002   0.605
dfh_v         65.434      5.983  10.937

Correlation of Fixed Effects:
      (Intr)
dfh_v -0.837
