import gspread
from oauth2client.service_account import ServiceAccountCredentials
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.http import HttpResponseServerError
from make_html import make_html
from . import gspread_auth


@csrf_exempt
def send(request):
    try:
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(gspread_auth.jsonkey_path, scope)
        gc = gspread.authorize(credentials)
        worksheet = gc.open_by_url(gspread_auth.gspread_url).worksheet("2nd")
        app_row = []
        prob = []
        for key in range(1, int(request.POST['qnum']) + 1):
            prob.append('answer' + str(key))
            prob.append('order' + str(key))
            prob.append('time' + str(key))
        # param_list = prob + ['order', 'id']
        param_list = prob + ['id','start','end']
        for param in param_list:
            app_row.append(request.POST[param])
        # OS,browser environment
        app_row.append(request.META['HTTP_USER_AGENT'])
        worksheet.append_row(app_row)



    except Exception as e:
        import traceback
        traceback.print_exc()
        return HttpResponseServerError()
    return HttpResponse(make_html("Succeed!"))


if __name__ == "__main__":
    send({}, True)
