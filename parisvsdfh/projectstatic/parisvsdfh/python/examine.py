# created ; 2018-04-18 16:30
# version : 1.0.1
# TODO AWS update
# TODO examine data update

import browser
from browser import timer, ajax, alert, html
import json
import random
import datetime

# 参考サイト
# https://qiita.com/ryo_grid/items/5e34220ed48f4580126d


# 保持変数の宣言
current_test_order = -1
users_answer = {}
current_test_page = -1
sample_num = 0
sample_data = {}
domain = "/".join(browser.window.location.href.split("/")[0:1])
users_answer["time"] = {}
users_answer["time"]["start"] = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
combination_dict = {"a": {"before": "p", "after": "q"},
                    "b": {"before": "notp", "after": "q"},
                    "c": {"before": "p", "after": "notq"},
                    "d": {"before": "notp", "after": "notq"},
                    }


def image_pre_load():
    global test_order
    start_button = browser.document['start_scenario_button']
    start_button.style = {"pointer-events": "none"}
    image_area = browser.document['preload_image']
    image_type = ["p", "notp", "q", "notq"]
    for image in test_order:
        for type in image_type:
            image_tag = html.IMG(src=image['samples']['images'][type])
            image_tag.style = {"display": "none"}
            image_area <= image_tag
    image_area.style = {"display": "none"}
    start_button.style = {"pointer-events": "all"}


def to_next_sample(ev=None):
    """ 事例提示時の次へボタンの挙動 """
    global current_test_page, sample_num
    if current_test_page >= sample_num:
        alert("終了しました。次に、解答をしてください。")
        drow_estimate()
        return
    else:
        select_next_sample()


def select_next_sample():
    global next_sample, current_test_page
    """次の事例を提示する"""
    # 次の例を選ぶ
    next_sample = select_random_sample(test_order[current_test_order])
    #   HTML要素を取得
    before_area = browser.document['sample_before']
    after_area = browser.document['sample_after']
    image_dict = test_order[current_test_order]["samples"]["images"]
    before_image = image_dict[combination_dict[next_sample]["before"]]
    after_image = image_dict[combination_dict[next_sample]["after"]]
    show_area = browser.document['show_area']
    show_area.style = {'display': 'none'}
    sample_description = browser.document['sample_description']
    first_description = browser.document['first_sentence']
    last_description = browser.document['last_sentence']
    sample_description.style = {'display': 'none'}
    # 現在何番目か表示する
    order_area = browser.document['order']
    sum = 0
    for _, sample_times in sample_data['samples']["frequency"].items():
        sum += sample_times
    # order_area.html = str(test_order[current_test_order]['sum'] - sum) + "/" + str(
    #     test_order[current_test_order]['sum'])
    order_area.html = str(test_order[current_test_order]['sum'] - sum) + "件目"

    def set_sample():
        # 画像を変更する
        before_area.src = before_image
        after_area.src = after_image
        # 説明文を差し替え
        pref, suff = test_order[current_test_order]['samples']['sentences'][next_sample].split("、")
        first_description.html = pref + '、'
        last_description.html = suff
        show_area.style = {'display': 'flex'}
        sample_description.style = {'display': 'inline-block'}

    timer.set_timeout(set_sample, 100)
    current_test_page += 1


def select_random_sample(sample_data: dict) -> str:
    """ ラベルと残りの回数がセットのdictを読みこんでランダムなパターンを返す """
    current_sample_selection = []
    for sample_tag, sample_times in sample_data["samples"]["frequency"].items():
        for times in range(sample_times):
            current_sample_selection.append(sample_tag)
    selection = random.choice(current_sample_selection)
    sample_data["samples"]["frequency"][selection] -= 1
    return selection


def drow_estimate():
    """ 推定画面を描画する """
    global current_test_order, test_order
    # 次のテストがあるかないか
    if current_test_order >= len(test_order) - 1:
        # なければ「解答を送信する」ボタンに名称変更
        next_button = browser.document['estimate_next_scenario']
        next_button.html = "回答を送信して<br>テストを終了する"
        browser.document["notes"].style = {"display": "inline-grid"}
    browser.document['estimate_gage'].value = 0
    browser.document['estimate'].text = 0
    show_area = browser.document['show_sample_area']
    estimate_area = browser.document['estimate_input_area']
    show_area.style = {'display': 'none'}
    estimate_area.style = {'display': 'inline-block'}
    estimate_description = browser.document['estimate_description']
    estimate_description.html = "<p><b>{}</b>はどのくらいでしょうか。</p>" \
                                "<p>0: 全く因果関係がない</p>" \
                                "<p>100: 完全に因果関係がある</p>" \
                                "<p>として、0から100の値で<b>直感で</b>答えてください。</p>".format(
        test_order[current_test_order]['samples']["result"])


def to_next_test(ev=None):
    """ 推定値評価後、次へボタンの挙動 """
    global current_test_order, users_answer
    user_estimate = browser.document["estimate"].value
    users_answer[sample_data['no']] = int(user_estimate)
    users_answer["time"][sample_data['no']] = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    # 終了か
    if not browser.document['estimate_next_scenario'].text == '次のシナリオへ進む':
        # 解答の保存
        button = browser.document["estimate_next_scenario"]
        button.html = "送信中です..."
        button.disabled = "true"
        save_users_answer()
    else:
        to_next_scenario_description()


def to_next_scenario_description():
    """ 次のテストの説明画面の描画 """
    global current_test_order, current_test_page, sample_num, test_order, sample_data
    current_test_order += 1
    current_test_page = 0
    sample_data = test_order[current_test_order]
    sample_num = [sample_sum for sample_sum in sample_data["samples"]['frequency']]
    # description
    desc_area = browser.document['description_area']
    estimate_area = browser.document['estimate_input_area']
    desc_area.style = {'display': 'inline-block'}
    estimate_area.style = {'display': 'none'}
    # write sentence
    title = browser.document['scenario_title']
    description = browser.document['scenario_description']
    title.html = test_order[current_test_order]['title']
    description.html = test_order[current_test_order]['description']


def to_next_new_sample_page(ev=None):
    """ 指定された順番(シャッフル後の順番)のテストのサンプル提示ページを読み込む,画面への出力 """
    global current_test_order, sample_num, test_order
    sample_num = 0
    for sample_sum in test_order[current_test_order]["samples"]['frequency'].values():
        sample_num += sample_sum
    # description
    desc_area = browser.document['description_area']
    sample_area = browser.document['show_sample_area']
    desc_area.style = {'display': 'none'}
    sample_area.style = {'display': 'inline-block'}
    # write sentence
    description = browser.document['scenario_description']
    description.html = test_order[current_test_order]['description']
    to_next_sample()


def read_sample_cases() -> list:
    f = open("../static/parisvsdfh/tests_data_2nd.json", "r")
    sample_str = f.read()
    f.close()
    samples = json.loads(sample_str)
    for sample in samples:
        sample['sum'] = 0
        for _, times in sample['samples']['frequency'].items():
            sample['sum'] += int(times)
    return samples


def save_users_answer():
    global test_order, users_answer, domain
    # ランダム数列を作成
    rand_id = random.randint(0, 99999999)
    users_answer['complete_id'] = str(rand_id).zfill(8)
    users_answer["time"]["end"] = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

    # 結果をGoogleスプレッドシートに追記
    # [prob.append(key) for key in range(1, len(users_answer.keys()))]
    # keys = prob + ['order', 'id']
    keys = list(users_answer.keys()) + ['id']
    # 記録する物
    # テスト順
    # 各テストに対する回答
    # ランダム数列
    send_dict = {}
    for obj in keys:
        if isinstance(obj, int):
            send_dict["answer" + str(obj)] = users_answer[obj]
            send_dict["order" + str(obj)] = test_order[obj - 1]['no']
            send_dict["time" + str(obj)] = users_answer["time"][obj]
        elif obj == "order":
            order = ""
            for num in range(len(test_order)):
                order += str(test_order[num]['no'])
            send_dict[obj] = order
        elif obj == "id":
            send_dict[obj] = users_answer['complete_id']
        elif obj == "time":
            send_dict["start"] = users_answer["time"]["start"]
            send_dict["end"] = users_answer["time"]["end"]
    send_dict["qnum"] = len(test_order)
    # send to google spread sheet
    url = "/".join([domain, "sendtoGS/"])
    # ajax
    req = ajax.ajax()
    req.bind('complete', on_send_complete)
    req.open('POST', url, True)
    req.set_header('content-type', 'application/x-www-form-urlencoded')
    req.send(send_dict)
    return


def on_send_complete(req):
    global users_answer, domain
    if req.status == 200 or req.status == 0:
        url = domain + '/end?id=' + users_answer['complete_id']
        browser.window.location.href = url
    else:
        alert("回答送信プロセスでエラーが発生しました。"
              "このページのまま少し時間を置いて再度お試しいただくか、問い合わせしていただきますようお願いします。")
        browser.document["estimate_next_scenario"].disabled = "false"

print("hello")
test_order = read_sample_cases()
random.shuffle(test_order)
tests_num = len(test_order)
# 説明エリア中身文章の差替え
image_pre_load()
to_next_scenario_description()
# 1->2
start_scenario = browser.document["start_scenario_button"]
start_scenario.bind("click", to_next_new_sample_page)
# 2->2 or 2->3
sub_elt = browser.document["next_sample"]
sub_elt.bind("click", to_next_sample)
# 3->1
estimate_to_description = browser.document["estimate_next_scenario"]
estimate_to_description.bind("click", to_next_test)
